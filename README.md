# edgeos

Welcome to your new module. A short overview of the generated parts can be found in the PDK documentation at https://puppet.com/pdk/latest/pdk_generating_modules.html .

The README template below provides a starting point with details about what information to include in your README.

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with edgeos](#setup)
    * [What edgeos affects](#what-edgeos-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with edgeos](#beginning-with-edgeos)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

**This is a work in progress.**

After getting frustrated by automating my Ubiquity(tm) Edge Routers with
Ansible (because of a paramiko issue), I figured that puppet now also has a device feature that can be
used. It was not too complicated (with the help of pdk), and here it is :)

## Setup

to run the examples, you need to write your own device.conf with
something like this in it:

    [ubnt8]
    type edgeos
    url  file:///home/<yourhome>/edgeos/spec/fixtures/ubnt.conf

and a fixtures file with the access config for your router:

spec/fixtures/ubnt.conf:

    # ubnt router:

    host: 192.168.1.1
    user: <user on router>
    password: <password>

then run examples as specified. Or, maybe adjust them a little first :D


### What edgeos affects **OPTIONAL**

currently, you can configure interfaces and firewall rules, port forward
rules are coming soon (when time permits).

the examples show what is possible at present.

### Setup Requirements **OPTIONAL**

Ahem. This Readme is also a work in progress...

If your module requires anything extra before setting up (pluginsync enabled, another module, etc.), mention it here.

If your most recent release breaks compatibility or requires particular steps for upgrading, you might want to include an additional "Upgrading" section here.

### Beginning with edgeos

The very basic steps needed for a user to get the module up and running. This can include setup steps, if necessary, or it can be an example of the most basic use of the module.

## Usage

Include usage examples for common use cases in the **Usage** section. Show your users how to use your module to solve problems, and be sure to include code examples. Include three to five examples of the most important or common tasks a user can accomplish with your module. Show users how to accomplish more complex tasks that involve different types, classes, and functions working in tandem.

## Reference

This section is deprecated. Instead, add reference information to your code as Puppet Strings comments, and then use Strings to generate a REFERENCE.md in your module. For details on how to add code comments and generate documentation with Strings, see the Puppet Strings [documentation](https://puppet.com/docs/puppet/latest/puppet_strings.html) and [style guide](https://puppet.com/docs/puppet/latest/puppet_strings_style.html)

If you aren't ready to use Strings yet, manually create a REFERENCE.md in the root of your module directory and list out each of your module's classes, defined types, facts, functions, Puppet tasks, task plans, and resource types and providers, along with the parameters for each.

For each element (class, defined type, function, and so on), list:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

For example:

```
### `pet::cat`

#### Parameters

##### `meow`

Enables vocalization in your cat. Valid options: 'string'.

Default: 'medium-loud'.
```

## Limitations

In the Limitations section, list any incompatibilities, known issues, or other warnings.

## Development

In the Development section, tell other users the ground rules for contributing to your project and how they should submit their work.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should consider using changelog). You can also add any additional sections you feel are necessary or important to include here. Please use the `## ` header.
