# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::EdgeosFirewallGroup')
require 'puppet/provider/edgeos_firewall_group/edgeos_firewall_group'

RSpec.describe Puppet::Provider::EdgeosFirewallGroup::EdgeosFirewallGroup do
  subject(:provider) { described_class.new }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:device) { instance_double('Puppet::Util::NetworkDevice::Edgeos', 'device') }

  before(:each) do
    allow(context).to receive(:debug).with('Fetch interfaces data')
    allow(context).to receive(:device).and_return(device)
    allow(device).to receive(:connection).and_return('connection')
  end

  describe '#get' do
    it 'processes resources' do
      expect(context).to receive(:debug).with('Returning firewall group configuration')
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall group"', 'connection'
      ).and_return(
        ['set firewall group network-group LAN_NETWORKS network 192.168.0.0/16',
         'set firewall group network-group LAN_NETWORKS network 172.16.0.0/12'],
      )
      expect(provider.get(context)).to eq [
        {
          ensure: 'present',
          name:   'LAN_NETWORKS',
          type:   'network-group',
          params: ['network 192.168.0.0/16', 'network 172.16.0.0/12'],
        },
      ]
    end
  end

  describe 'create(context, name, should)' do
    it 'creates the resource' do
      expect(context).to receive(:notice).with('Creating \'DNS_SERVERS\': ["set firewall group address-group DNS_SERVERS address 8.8.8.8"]')
      expect(device).to receive(:configure).with(context, ['set firewall group address-group DNS_SERVERS address 8.8.8.8'])

      provider.create(context, 'DNS_SERVERS',
                      name:   'DNS_SERVERS',
                      ensure: 'present',
                      type:   'address-group',
                      params: ['address 8.8.8.8'])
    end
  end

  describe 'update(context, name, should)' do
    it 'updates the resource' do
      expect(context).to receive(:debug).with('Returning firewall group configuration')
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall group"', 'connection'
      ).and_return(
        ['set firewall group address-group DNS_SERVERS address 1.1.1.1'],
      )
      expect(context).to receive(:notice).with(%r{\AUpdating 'DNS_SERVERS'})
      expect(device).to receive(:configure).with(
        context, ['delete firewall group address-group DNS_SERVERS address 1.1.1.1',
                  'set firewall group address-group DNS_SERVERS address 8.8.8.8']
      )

      provider.update(context, 'DNS_SERVERS',
                      name:   'DNS_SERVERS',
                      ensure: 'present',
                      type:   'address-group',
                      params: ['address 8.8.8.8'])
    end
  end

  describe 'delete(context, name)' do
    it 'deletes the resource' do
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall group"', 'connection'
      ).and_return(
        ['set firewall group address-group DNS_SERVERS address 1.1.1.1'],
      )
      expect(context).to receive(:debug).with('Returning firewall group configuration')
      expect(context).to receive(:notice).with(%r{\ADeleting 'DNS_SERVERS'})
      expect(device).to receive(:configure).with(
        context, ['delete firewall group address-group DNS_SERVERS']
      )
      provider.delete(context, 'DNS_SERVERS')
    end
  end
end
