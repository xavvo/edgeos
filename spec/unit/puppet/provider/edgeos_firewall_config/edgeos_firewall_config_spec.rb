# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::EdgeosFirewallConfig')
require 'puppet/provider/edgeos_firewall_config/edgeos_firewall_config'

RSpec.describe Puppet::Provider::EdgeosFirewallConfig::EdgeosFirewallConfig do
  subject(:provider) { described_class.new }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:device) { instance_double('Puppet::Util::NetworkDevice::Edgeos', 'device') }

  before(:each) do
    allow(context).to receive(:debug).with('Fetch interfaces data')
    allow(context).to receive(:device).and_return(device)
    allow(device).to receive(:connection).and_return('connection')
  end

  describe '#get' do
    it 'processes resources' do
      expect(context).to receive(:debug).with('Returning firewall general config')
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall"', 'connection'
      ).and_return(
        ['set firewall ipv6-receive-redirects disable',
         'set firewall log-martians enable'],
      )
      expect(provider.get(context)).to eq [
        {
          name: 'ipv6-receive-redirects',
          ensure: 'present',
          state: 'disable',
        },
        {
          name: 'log-martians',
          ensure: 'present',
          state: 'enable',
        },
      ]
    end
  end

  describe 'create(context, name, should)' do
    it 'creates the resource' do
      expect(context).to receive(:debug).with(%r{\ACreating 'all-ping'})
      expect(device).to receive(:configure).with(context, ['set firewall all-ping enable'])

      provider.create(context, 'all-ping', name: 'all-ping', ensure: 'present', state: 'enable')
    end
  end

  describe 'update(context, name, should)' do
    it 'updates the resource' do
      expect(context).to receive(:debug).with(%r{\AUpdating 'syn-cookies'})
      expect(device).to receive(:configure).with(context, ['set firewall syn-cookies enable'])

      provider.update(context, 'syn-cookies', name: 'syn-cookies', ensure: 'present', state: 'enable')
    end
  end

  describe 'delete(context, name)' do
    it 'deletes the resource' do
      expect(context).to receive(:debug).with(%r{\ADeleting 'ipv6-src-route'})
      expect(device).to receive(:configure).with(context, ['delete firewall ipv6-src-route'])

      provider.delete(context, 'ipv6-src-route')
    end
  end
end
