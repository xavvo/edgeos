# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::EdgeosFirewallRules')
require 'puppet/provider/edgeos_firewall_rules/edgeos_firewall_rules'

RSpec.describe Puppet::Provider::EdgeosFirewallRules::EdgeosFirewallRules do
  subject(:provider) { described_class.new }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:device) { instance_double('Puppet::Util::NetworkDevice::Edgeos', 'device') }

  before(:each) do
    allow(context).to receive(:debug).with('Return firewall rules data')
    allow(context).to receive(:device).and_return(device)
    allow(device).to receive(:connection).and_return('connection')
  end

  describe '#get' do
    it 'processes resources' do
      expect(context).to receive(:debug).with(%r{\AGet ruleset })
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall \(ipv6-\)\{0,1\}name"',
        'connection',
      ).and_return(
        ['set firewall name WAN_IN default-action drop',
         'set firewall name WAN_IN description \'WAN to internal\'',
         'set firewall name WAN_IN rule 10 action accept',
         'set firewall name WAN_IN rule 10 description \'Allow established/related\'',
         'set firewall name WAN_IN rule 10 state established enable',
         'set firewall name WAN_IN rule 10 state related enable',
         'set firewall name WAN_IN rule 20 action drop',
         'set firewall name WAN_IN rule 20 description \'Drop invalid state\'',
         'set firewall name WAN_IN rule 20 state invalid enable',
         'set firewall name WAN_IN rule 30 action accept',
         'set firewall name WAN_IN rule 30 description \'Allow Wireguard\'',
         'set firewall name WAN_IN rule 30 destination port 5182',
         'set firewall name WAN_IN rule 30 protocol udp'],
      )
      expect(provider.get(context)).to eq [
        {
          name: 'WAN_IN',
          ensure: 'present',
          type: 'ipv4',
          default_action: 'drop',
          default_log: 'disable',
          description: '\'WAN to internal\'',
          rules: {
            '10' => ['action accept', 'description \'Allow established/related\'', 'state established enable', 'state related enable'],
            '20' => ['action drop', 'description \'Drop invalid state\'', 'state invalid enable'],
            '30' => ['action accept', 'description \'Allow Wireguard\'', 'destination port 5182', 'protocol udp'],
          },
        },
      ]
    end
  end

  describe 'create(context, name, should)' do
    it 'creates the resource' do
      expect(context).to receive(:debug).with(%r{\ACreating 'WAN_LOCAL':})
      expect(device).to receive(:configure).with(context,
                                                 ['set firewall name WAN_LOCAL default-action drop',
                                                  'set firewall name WAN_LOCAL rule 10 action drop',
                                                  'set firewall name WAN_LOCAL rule 10 state invalid enable',
                                                  'set firewall name WAN_LOCAL rule 20 action accept',
                                                  'set firewall name WAN_LOCAL rule 20 state related enable'])

      provider.create(context, 'WAN_LOCAL', name:           'WAN_LOCAL',
                                            ensure:         'present',
                                            type:           'ipv4',
                                            default_action: 'drop',
                                            rules: {
                                              '10' => ['action drop', 'state invalid enable'],
                                              '20' => ['action accept', 'state related enable'],
                                            })
    end
  end

  describe 'update(context, name, should)' do
    it 'updates the resource' do
      expect(context).to receive(:debug).with(%r{\AGet ruleset})
      expect(context).to receive(:debug).with(%r{\AUpdating 'WAN_IN' with})
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall \(ipv6-\)\{0,1\}name"',
        'connection',
      ).and_return(
        ['set firewall name WAN_IN default-action drop',
         'set firewall name WAN_IN description \'WAN to internal\'',
         'set firewall name WAN_IN rule 10 action accept',
         'set firewall name WAN_IN rule 10 description \'Allow established/related\'',
         'set firewall name WAN_IN rule 10 state established enable',
         'set firewall name WAN_IN rule 10 state related enable',
         'set firewall name WAN_IN rule 20 action drop',
         'set firewall name WAN_IN rule 20 description \'Drop invalid state\'',
         'set firewall name WAN_IN rule 20 state invalid enable',
         'set firewall name WAN_IN rule 30 action accept',
         'set firewall name WAN_IN rule 30 description \'Allow Wireguard\'',
         'set firewall name WAN_IN rule 30 destination port 5182',
         'set firewall name WAN_IN rule 30 protocol udp'],
      )
      expect(device).to receive(:configure).with(context,
                                                 ['delete firewall name WAN_IN rule 10 description \'Allow established/related\'',
                                                  'delete firewall name WAN_IN rule 30 action accept',
                                                  'delete firewall name WAN_IN rule 30 description \'Allow Wireguard\'',
                                                  'delete firewall name WAN_IN rule 30 destination port 5182',
                                                  'delete firewall name WAN_IN rule 30 protocol udp',
                                                  'set firewall name WAN_IN rule 40 action accept',
                                                  'set firewall name WAN_IN rule 40 description \'Allow Openvpn\'',
                                                  'set firewall name WAN_IN rule 40 destination port 1194',
                                                  'set firewall name WAN_IN rule 40 protocol udp'])

      provider.update(context, 'WAN_IN', name:           'WAN_IN',
                                         ensure:         'present',
                                         type:           'ipv4',
                                         description:    '\'WAN to internal\'',
                                         default_action: 'drop',
                                         rules: {
                                           '10' => ['action accept', 'state established enable', 'state related enable'],
                                           '20' => ['action drop', 'description \'Drop invalid state\'', 'state invalid enable'],
                                           '40' => ['action accept', 'description \'Allow Openvpn\'', 'destination port 1194', 'protocol udp'],
                                         })
    end
  end

  describe 'delete(context, name)' do
    it 'deletes the resource' do
      expect(context).to receive(:debug).with(%r{\AGet ruleset})
      expect(context).to receive(:debug).with(%r{\ADeleting 'WAN_IN'})
      expect(device).to receive(:command).with(
        'show configuration commands |grep "^set firewall \(ipv6-\)\{0,1\}name"',
        'connection',
      ).and_return(
        ['set firewall name WAN_IN default-action drop',
         'set firewall name WAN_IN description \'WAN to internal\''],
      )
      expect(device).to receive(:configure).with(context, ['delete firewall name WAN_IN'])

      provider.delete(context, 'WAN_IN')
    end
  end
end
