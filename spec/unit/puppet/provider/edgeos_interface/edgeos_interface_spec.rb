# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::EdgeosInterface')
require 'puppet/provider/edgeos_interface/edgeos_interface'

RSpec.describe Puppet::Provider::EdgeosInterface::EdgeosInterface do
  subject(:provider) { described_class.new }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:device) { instance_double('Puppet::Util::NetworkDevice::Edgeos', 'device') }

  before(:each) do
    allow(context).to receive(:debug).with('Fetch interfaces data')
    allow(context).to receive(:device).and_return(device)
    allow(device).to receive(:connection).and_return('connection')
  end

  describe '#get' do
    it 'processes resources' do
      expect(device).to receive(:command).with('show configuration commands |grep interfaces', 'connection').and_return(
        ['set interfaces ethernet eth1 description vlan10',
         'set interfaces ethernet eth1 duplex auto',
         'set interfaces ethernet eth1 speed auto',
         'set interfaces ethernet eth2 description vlan20',
         'set interfaces ethernet eth2 duplex auto',
         'set interfaces ethernet eth2 speed auto'],
      )
      expect(provider.get(context)).to eq [
        {
          ensure: 'present',
          name:   'eth1',
          type:   'ethernet',
          params: ['description vlan10', 'duplex auto', 'speed auto'],
        },
        {
          ensure: 'present',
          name:   'eth2',
          type:   'ethernet',
          params: ['description vlan20', 'duplex auto', 'speed auto'],
        },
      ]
    end
  end

  describe 'create(context, name, should)' do
    it 'successfully creates a resource' do
      expect(context).to receive(:notice).with('Create tun0: ["set interfaces tunnel tun0 address 1.2.3.4/24"]')
      expect(device).to receive(:configure).with(context, ['set interfaces tunnel tun0 address 1.2.3.4/24'])

      provider.create(context, 'tun0', name: 'tun0', type: 'tunnel', ensure: 'present', params: ['address 1.2.3.4/24'])
    end
  end

  describe 'update(context, name, should)' do
    it 'updates the resource' do
      expect(device).to receive(:command).with('show configuration commands |grep interfaces', 'connection').and_return(
        ['set interfaces ethernet eth2 duplex auto',
         'set interfaces ethernet eth2 speed auto'],
      )
      expect(context).to receive(:notice).with('Update eth2: ["delete interfaces ethernet eth2 duplex auto", "delete interfaces ethernet eth2 speed auto"]')
      expect(device).to receive(:configure).with(context, ['delete interfaces ethernet eth2 duplex auto', 'delete interfaces ethernet eth2 speed auto'])

      provider.update(context, 'eth2', name: 'eth2', ensure: 'present', type: 'ethernet', params: [])
    end
  end

  describe 'delete(context, name)' do
    it 'deletes the resource' do
      expect(device).to receive(:command).with('show configuration commands |grep interfaces', 'connection').and_return(
        ['set interfaces tunnel tun0 duplex auto',
         'set interfaces tunnel tun0 speed auto'],
      )
      expect(context).to receive(:notice).with('Delete tun0: "delete interfaces tunnel tun0"')
      expect(device).to receive(:configure).with(context, 'delete interfaces tunnel tun0')

      provider.delete(context, 'tun0')
    end
  end
end
