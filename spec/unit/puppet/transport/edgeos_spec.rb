# frozen_string_literal: true

require 'spec_helper'
require 'puppet/transport/edgeos'
require 'puppet/resource_api/puppet_context'
require 'net/ssh/telnet'

RSpec.describe Puppet::Transport::Edgeos do
  subject(:transport) { described_class.new(context, connection_info) }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:password) { 'aih6cu6ohvohpahN' }
  let(:connection_info) do
    {
      host: 'api.example.com',
      user: 'admin',
      password: Puppet::Pops::Types::PSensitiveType::Sensitive.new(password),
    }
  end
  let(:ssh_session) { instance_double('Net::SSH::Connection::Session', 'ssh_session') }
  let(:telnet_session) { instance_double('Net::SSH::Telnet', 'telnet_session') }
  let(:result) { '' }

  before(:each) do
    allow(context).to receive(:debug)
    allow(Net::SSH).to receive(:start).and_return(ssh_session)
    allow(Net::SSH::Telnet).to receive(:new).and_return(telnet_session)
    allow(telnet_session).to receive(:cmd).and_return(result)
    allow(telnet_session).to receive(:ssh).and_return(ssh_session)
    allow(telnet_session).to receive(:close).and_return(nil)
    allow(ssh_session).to receive(:host).and_return('bog.com')
    allow(ssh_session).to receive(:closed?).and_return(false)
  end

  describe 'initialize(context, connection_info)' do
    it { expect { transport }.not_to raise_error }
  end

  describe 'verify(context)' do
    context 'with valid credentials' do
      it 'returns' do
        expect { transport.verify(context) }.not_to raise_error
      end
    end
  end

  describe 'facts(context)' do
    let(:facts) { transport.facts(context) }

    it 'returns basic facts' do
      expect(facts).to include('version' => nil, 'config' => nil)
    end
  end

  describe 'close(context)' do
    it 'releases resources' do
      transport.close(context)

      expect(transport.instance_variable_get(:@connection_info)).to be_nil
    end
  end
end
