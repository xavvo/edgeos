# frozen_string_literal: true

require 'spec_helper'
require 'puppet/transport/schema/edgeos'

RSpec.describe 'the edgeos transport' do
  it 'loads' do
    expect(Puppet::ResourceApi::Transport.list['edgeos']).not_to be_nil
  end
end
