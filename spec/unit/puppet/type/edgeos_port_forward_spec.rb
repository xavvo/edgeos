# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/edgeos_port_forward'

RSpec.describe 'the edgeos_port_forward type' do
  it 'loads' do
    expect(Puppet::Type.type(:edgeos_port_forward)).not_to be_nil
  end
end
