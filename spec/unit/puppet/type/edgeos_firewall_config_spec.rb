# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/edgeos_firewall_config'

RSpec.describe 'the edgeos_firewall_config type' do
  it 'loads' do
    expect(Puppet::Type.type(:edgeos_firewall_config)).not_to be_nil
  end
end
