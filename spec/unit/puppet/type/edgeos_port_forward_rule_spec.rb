# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/edgeos_port_forward_rule'

RSpec.describe 'the edgeos_port_forward_rule type' do
  it 'loads' do
    expect(Puppet::Type.type(:edgeos_port_forward_rule)).not_to be_nil
  end
end
