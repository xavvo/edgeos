# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/edgeos_interface'

RSpec.describe 'the edgeos_interface type' do
  it 'loads' do
    expect(Puppet::Type.type(:edgeos_interface)).not_to be_nil
  end
end
