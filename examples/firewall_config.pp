# set a few configs in firewall:

edgeos_firewall_config { [ 'ip-src-route', 'ipv6-src-route', 'broadcast-ping', ]:
  state => 'disable',
}
