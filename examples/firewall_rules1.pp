# standard rules

edgeos_firewall_rules { 'WAN_IN':
  ensure         => 'present',
  type           => 'ipv4',
  default_log    => 'disable',
  default_action => 'drop',
  description    => '\'WAN to internal\'',
  rules          => {
    '10' => ['action accept', 'description \'Allow established/related\'', 'state established enable', 'state related enable'],
    '20' => ['action drop', 'description \'Drop invalid state\'', 'state invalid enable']
  },
}
edgeos_firewall_rules { 'WAN_LOCAL':
  ensure         => 'present',
  type           => 'ipv4',
  default_log    => 'disable',
  default_action => 'drop',
  description    => '\'WAN to router\'',
  rules          => {
    '10' => ['action accept', 'description \'Allow established/related\'', 'state established enable', 'state related enable'],
    '20' => ['action drop', 'description \'Drop invalid state\'', 'state invalid enable'],
    '21' => ['action accept', 'description \'Allow Ping\'', 'destination group address-group NETv4_eth0', 'log disable', 'protocol icmp']
  },
}
edgeos_firewall_rules { 'home-ipv6':
  ensure         => 'present',
  type           => 'ipv6',
  default_log    => 'disable',
  default_action => 'drop',
  rules          => {
    '10' => ['action accept', 'state established enable', 'state related enable'],
    '20' => ['action accept', 'log disable', 'protocol icmpv6']
  },
}
