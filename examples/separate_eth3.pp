# take eth3 out of switch0 and give it an independent address

# call with:
# pdk bundle exec puppet device -d --libdir lib --modulepath /etc/puppetlabs/code/environments/production/modules \
# --deviceconfig device.conf --target ubntx --apply examples/separate_eth3.pp

$port        = 'eth3'
$address     = '10.10.10.1/16'
$description = "\'special usage\'"

$switch_lines = $facts['config'].filter |String $line| {
  $line =~ '^set interfaces switch switch0' and
  $line !~ "^set interfaces switch switch0 switch-port interface ${port}\$"
}
debug($switch_lines)

$switch_params = $switch_lines.map |String $line| {
  regsubst($line, '^set interfaces switch switch0 (.*)$', '\\1')
}
debug($switch_params)

notice("ensure port ${port} is not on switch0")
edgeos_interface { 'switch0':
  ensure => 'present',
  type   => 'switch',
  params => $switch_params,
}

notice("configure address ${address} for ${port}")
edgeos_interface { $port :
  ensure => 'present',
  type   => 'ethernet',
  params => [
    "address ${address}",
    "description ${description}",
    'duplex auto',
    'speed auto',
  ]
}
