# setup tun0 as a ip6 tunnel interface

edgeos_interface { 'tun0':
  ensure => 'present',
  type   => 'tunnel',
  params => [
    'address \'2a0c:3b80:7b00:d9::2/64\'',
    'description \'Securebit IPv6 Tunnel\'',
    'encapsulation sit',
    'firewall in ipv6-name home-ipv6',
    'firewall local ipv6-name home-ipv6',
    'local-ip 0.0.0.0',
    'multicast disable',
    'remote-ip 149.11.89.68',
    'ttl 255'
  ],
}

