# take eth3 back into switch0
#
# call with:
# pdk bundle exec puppet device -d --libdir lib --modulepath /etc/puppetlabs/code/environments/production/modules \
# --deviceconfig device.conf --target ubntx --apply examples/integrate_eth3.pp

$port        = 'eth3'
$description = 'Local'

$switch_lines = $facts['config'].filter |String $line| {
  $line =~ '^set interfaces switch switch0'
}
debug($switch_lines)

$switch_params = $switch_lines.map |String $line| {
  regsubst($line, '^set interfaces switch switch0 (.*)$', '\\1')
}
debug($switch_params)

notice("configure ${port} for the switch")
edgeos_interface { $port :
  ensure => 'present',
  type   => 'ethernet',
  params => [
    "description ${description}",
    'duplex auto',
    'speed auto',
  ]
}

notice("ensure port ${port} is in switch0")
edgeos_interface { 'switch0':
  ensure => 'present',
  type   => 'switch',
  params => union($switch_params,["switch-port interface ${port}"]),
}
