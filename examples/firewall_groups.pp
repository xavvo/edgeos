# try some firewall groups

edgeos_firewall_group {'LAN_NETWORKS':
  type   => network-group,
  params => [
    'description \'RFC1918 ranges\'',
    'network 192.168.0.0/16',
    'network 172.16.0.0/12',
    'network 10.0.0.0/8',
  ]
}

edgeos_firewall_group {'EXTERNAL_IP6_ADDRESS':
  ensure => absent,
  type   => 'ipv6-address-group',
}

edgeos_firewall_group {'IP6_DNS_SERVERS':
  type   => 'ipv6-address-group',
  params => [
    'description \'CloudFlare DNS\'',
    'ipv6-address \'2606:4700:4700::1111\'',
    'ipv6-address \'2606:4700:4700::1001\'',
  ]
}
