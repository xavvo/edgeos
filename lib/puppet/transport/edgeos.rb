# frozen_string_literal: true

require 'net/ssh'
require 'net/ssh/telnet'

module Puppet::Transport
  # transport class for edgeos routers
  class Edgeos
    attr_reader :connection

    def initialize(context, connection_info)
      port = connection_info[:port].nil? ? 22 : connection_info[:port]
      context.debug("Open a ssh-telnet connection to #{connection_info[:host]}:#{port}.")
      dump_log = connection_info[:dump_log].nil? ? '/tmp/dumplog.log' : connection_info[:dump_log]

      ssh = Net::SSH.start(connection_info[:host], connection_info[:user],
                           port: port,
                           password: connection_info[:password].unwrap)
      @connection = Net::SSH::Telnet.new(
        'Session'  => ssh,
        'Dump_log' => dump_log,
      )
    end

    # Verifies that the stored credentials are valid, and that we can talk to the target

    def verify(context)
      context.debug("Checking connection to #{@connection.ssh.host}")
      raise 'connection closed' if @connection.ssh.closed?
    end

    # Retrieve facts from the target and return in a hash

    def facts(context)
      context.debug('Retrieving facts')
      # disable pageing
      @connection.cmd('terminal length 0')
      version = @connection.cmd('show version').split("\n")[1..-2]
      config = @connection.cmd('show configuration commands').split("\n")[1..-2]
      {
        'version' => version,
        'config'  => config,
      }
    end

    # put a command and return its result

    def command(command, connection)
      connection.cmd('terminal length 0')
      r = connection.cmd(command).split("\n")
      r[1..-2]
    end

    # send configuration line(s) and commit

    def configure(context, lines, save = false)
      @connection.cmd('terminal length 0')
      @connection.cmd('configure')
      lines.each do |line|
        context.debug("Configure: '#{line}'")
        cmd_result = @connection.cmd(line)
        if cmd_result =~ %r{failed}
          raise Puppet::ResourceError, cmd_result
        end
      end
      commit_result = @connection.cmd('commit')
      if commit_result =~ %r{failed}
        @connection.cmd('exit discard')
        raise Puppet::ResourceError, commit_result
      end
      @connection.cmd('save') if save
      @connection.cmd('exit')
    end

    # Close the connection and release all resources

    def close(context)
      context.debug('Closing connection')
      @connection.close
      @connection = nil
    end
  end
end
