# frozen_string_literal: true

require 'puppet/resource_api/simple_provider'
require 'pry'

# Implementation for the edgeos_firewall_rules type using the Resource API.
class Puppet::Provider::EdgeosFirewallRules::EdgeosFirewallRules < Puppet::ResourceApi::SimpleProvider
  def get(context)
    context.debug('Return firewall rules data')
    firewall_list = context.device.command(
      'show configuration commands |grep "^set firewall \(ipv6-\)\{0,1\}name"',
      context.device.connection,
    )
    ruleset = {}
    firewall_list.each do |config_line|
      el = config_line.split
      name = el[3]
      ruleset[name] = {} unless ruleset[name]
      ruleset[name][:ensure] = 'present'
      ruleset[name][:name] = name
      ruleset[name][:type] = (el[2] == 'name') ? 'ipv4' : 'ipv6'
      ruleset[name][:default_log] = 'disable' unless ruleset[name][:default_log]
      case el[4]
      when 'default-action'
        ruleset[name][:default_action] = el[5]
      when 'description'
        ruleset[name][:description] = el[5..-1].join(' ')
      when 'enable-default-log'
        ruleset[name][:default_log] = 'enable'
      when 'rule'
        ruleset[name][:rules] = {} unless ruleset[name][:rules]
        ruleset[name][:rules][el[5]] = [] unless ruleset[name][:rules][el[5]]
        ruleset[name][:rules][el[5]] << el[6..-1].join(' ')
      end
    end
    context.debug("Get ruleset #{ruleset.inspect}")
    ruleset.map { |_, j| j }
  end

  def create(context, name, should)
    commands = []
    type = (should[:type] == 'ipv4') ? 'name' : 'ipv6-name'
    command = "set firewall #{type} #{name}"
    commands << "#{command} default-action #{should[:default_action]}"
    commands << "#{command} description #{should[:description]}" if should[:description]
    commands << "#{command} enable-default-log" if should[:default_log] == 'enable'
    should[:rules].each do |rule, params|
      params.each do |s|
        commands << "#{command} rule #{rule} #{s}"
      end
    end
    context.debug("Creating '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def update(context, name, should)
    is = get(context).find { |r| r[:name] == name }
    commands = []
    type = (should[:type] == 'ipv4') ? 'name' : 'ipv6-name'
    command = "set firewall #{type} #{name}"
    if is[:type] != should[:type]
      oldtype = (is[:type] == 'ipv4') ? 'name' : 'ipv6-name'
      commands << "delete firewall #{oldtype} #{name}"
      is = {}
      is[:rules] = {}
    end
    if is[:default_log] == 'enable' && should[:default_log] == 'disable'
      commands << "delete firewall #{type} #{name} enable-default-log"
    end
    is[:rules].each do |rule, params|
      params.each do |p|
        commands << "delete firewall #{type} #{name} rule #{rule} #{p}" unless
          should[:rules][rule] && should[:rules][rule].include?(p)
      end
    end
    [:default_action, :description, :default_log].each do |a|
      case a
      when :default_action
        params = "default-action #{should[a]}"
      when :description
        params = (should[a]) ? "description #{should[a]}" : nil
      when :default_log
        params = 'enable-default-log' if should[:default_log] == 'enable'
      end
      commands << "#{command} #{params}" unless params.nil? || is[a] == should[a]
    end
    should[:rules].each do |rule, params|
      params.each do |p|
        commands << "#{command} rule #{rule} #{p}" unless is[:rules][rule] && is[:rules][rule].include?(p)
      end
    end
    context.debug("Updating '#{name}' with #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def delete(context, name)
    context.debug("Deleting '#{name}'")
    is = get(context).find { |r| r[:name] == name }
    type = (is[:type] == 'ipv4') ? 'name' : 'ipv6-name'
    context.device.configure(context, ["delete firewall #{type} #{name}"])
  end
end
