# frozen_string_literal: true

require 'puppet/resource_api/simple_provider'

# Implementation for the edgeos_interface type using the Resource API.
class Puppet::Provider::EdgeosInterface::EdgeosInterface < Puppet::ResourceApi::SimpleProvider
  def get(context)
    context.debug('Fetch interfaces data')
    interfaces_list = context.device.command(
      'show configuration commands |grep interfaces',
      context.device.connection,
    )
    interfaces = {}
    interfaces_list.each do |config_line|
      el = config_line.split
      type = el[2]
      name = el[3]
      param = el[4..-1].join(' ')
      interfaces[name] = {} unless interfaces[name]
      interfaces[name][:ensure] = 'present'
      interfaces[name][:name] = name
      interfaces[name][:type] = type
      interfaces[name][:params] = [] unless interfaces[name][:params]
      interfaces[name][:params] << param
    end
    interfaces.map { |_, j| j }
  end

  def create(context, name, should)
    commands = []
    should[:params].each do |p|
      commands << "set interfaces #{should[:type]} #{should[:name]} #{p}"
    end
    context.notice("Create #{name}: #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def update(context, name, should)
    is = get(context).find { |r| r[:name] == name }
    commands = []
    is[:params].each do |k|
      commands << "delete interfaces #{is[:type]} #{is[:name]} #{k}" unless should[:params].include? k
    end
    should[:params].each do |l|
      commands << "set interfaces #{should[:type]} #{should[:name]} #{l}" unless is[:params].include? l
    end
    context.notice("Update #{name}: #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def delete(context, name)
    is = get(context).find { |r| r[:name] == name }
    commands = "delete interfaces #{is[:type]} #{is[:name]}"
    context.notice("Delete #{name}: #{commands.inspect}")
    context.device.configure(context, commands)
  end
end
