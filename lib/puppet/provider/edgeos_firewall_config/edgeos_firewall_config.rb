# frozen_string_literal: true

require 'puppet/resource_api/simple_provider'

# Implementation for the edgeos_firewall_config type using the Resource API.
class Puppet::Provider::EdgeosFirewallConfig::EdgeosFirewallConfig < Puppet::ResourceApi::SimpleProvider
  def get(context)
    context.debug('Returning firewall general config')
    firewall_list = context.device.command(
      'show configuration commands |grep "^set firewall"',
      context.device.connection,
    )
    firewall_config = []
    firewall_list.each do |config_line|
      el = config_line.split
      next if ['name', 'ipv6-name', 'group', 'modify', 'ipv6-modify', 'options'].include? el[2]
      firewall_config << {
        ensure: 'present',
        name:   el[2],
        state:  el[3],
      }
    end
    firewall_config
  end

  def create(context, name, should)
    commands = ["set firewall #{name} #{should[:state]}"]
    context.debug("Creating '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def update(context, name, should)
    commands = ["set firewall #{name} #{should[:state]}"]
    context.debug("Updating '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def delete(context, name)
    commands = ["delete firewall #{name}"]
    context.debug("Deleting '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end
end
