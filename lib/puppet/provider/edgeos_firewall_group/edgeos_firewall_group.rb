# frozen_string_literal: true

require 'puppet/resource_api/simple_provider'

# Implementation for the edgeos_firewall_group type using the Resource API.
class Puppet::Provider::EdgeosFirewallGroup::EdgeosFirewallGroup < Puppet::ResourceApi::SimpleProvider
  def get(context)
    context.debug('Returning firewall group configuration')
    firewall_list = context.device.command(
      'show configuration commands |grep "^set firewall group"',
      context.device.connection,
    )
    firewall_group = {}
    firewall_list.each do |config_line|
      el = config_line.split
      name = el[4]
      firewall_group[name] = {} unless firewall_group[name]
      firewall_group[name][:ensure] = 'present'
      firewall_group[name][:type]   = el[3]
      firewall_group[name][:name]   = name
      firewall_group[name][:params] = [] unless firewall_group[name][:params]
      firewall_group[name][:params] << el[5..-1].join(' ')
    end
    firewall_group.map { |_, j| j }
  end

  def create(context, name, should)
    commands = []
    should[:params].each do |p|
      commands << "set firewall group #{should[:type]} #{name} #{p}"
    end
    context.notice("Creating '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def update(context, name, should)
    is = get(context).find { |r| r[:name] == name }
    commands = []
    is[:params].each do |k|
      commands << "delete firewall group #{is[:type]} #{is[:name]} #{k}" unless should[:params].include? k
    end
    should[:params].each do |l|
      commands << "set firewall group #{is[:type]} #{is[:name]} #{l}" unless is[:params].include? l
    end
    context.notice("Updating '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end

  def delete(context, name)
    is = get(context).find { |r| r[:name] == name }
    commands = ["delete firewall group #{is[:type]} #{name}"]
    context.notice("Deleting '#{name}': #{commands.inspect}")
    context.device.configure(context, commands)
  end
end
