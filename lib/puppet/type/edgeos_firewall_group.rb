# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_firewall_group',
  docs: <<-EOS,
@summary a edgeos_firewall_group type
@example
edgeos_firewall_group { 'foo':
  ensure => 'present',
}

This type provides Puppet with the capabilities to manage firewall groups,
i.e. groups of addresses, networks or ports that can be used in rules.
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    type: {
      type: 'Enum[address-group, ipv6-address-group, ipv6-network-group, network-group, port-group]',
      desc: 'The type of group to define',
    },
    params: {
      type: 'Array[String]',
      desc: 'the list of configuration strings',
    },
  },
)
