# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_port_forward_rule',
  docs: <<-EOS,
@summary a edgeos_port_forward_rule type
@example
edgeos_port_forward_rule { '1':
  ensure        => 'present',
  description   => 'forward http to 1.2.3.4',
  original_port => '80',
  forward_to    => ['192.168.1.5', '8888'],
}

This type provides Puppet with the capabilities to manage port forwarding rules.
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the rule you want to manage.',
      behaviour: :namevar,
    },
    description: {
      type:      'String',
      desc:      'What the rule is used for.',
    },
    original_port: {
      type:      'Variant[Integer, String]',
      desc:      'The port number or name that gets forwarded',
    },
    forward_to: {
      type:      'Array[String]',
      desc:      'Destination address and port',
    },
  },
)
