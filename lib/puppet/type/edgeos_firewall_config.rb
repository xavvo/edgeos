# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_firewall_config',
  docs: <<-EOS,
@summary a edgeos_firewall_config type
@example
edgeos_firewall_config { 'broadcast_ping':
  state => 'disable',
}
edgeos_firewall_config { 'options':
  value => 'mss-clamp interface-type all',
}

This type provides Puppet with the capabilities to manage basic firewal config
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    state: {
      type:     'Enum[enable, disable]',
      desc:     'Wether the feature is enabled or disabled',
    },
    value: {
      type:     'String',
      desc:     'The value of the feature',
    },
  },
)
