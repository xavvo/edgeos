# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_firewall_rules',
  docs: <<-EOS,
@summary a edgeos_firewall_rules type
@example
edgeos_firewall_rules { 'WAN_IN':
  ensure         => 'present',
  default_action => 'accept',
  description    => 'WAN to internal',
  rules          => {
    '10' => ['action accept',
             'state established enable',
             'state related enable',
            ],
    '20' => ['action drop',
             'description "Drop invalid state"',
             'state invalid enable',
            ],
  },
}

This type provides Puppet with the capabilities to manage an edgeos firewall ruleset.
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    type: {
      type:      'Enum[ipv4, ipv6]',
      desc:      'The name of the resource you want to manage.',
    },
    default_action: {
      type: 'Enum[accept, drop, reject]',
      desc: 'The default action for the entire ruleset',
    },
    description: {
      type: 'Optional[String]',
      desc: 'The description for the ruleset',
    },
    default_log: {
      type:    'Enum[enable, disable]',
      desc:    'Wether default logging is enabled',
      default: 'disable',
    },
    rules: {
      type: 'Hash[String, Array]',
      desc: 'The list of firewall rules',
    },
  },
)
