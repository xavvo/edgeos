# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_interface',
  docs: <<-EOS,
@summary a edgeos_interface type
@example
edgeos_interface { 'eth0':
  ensure => 'present',
  type   => 'ethernet',
  params => {
    description => 'Internet',
    address     => 'dhcp',
    duplex      => 'auto',
    firewall    => {
      in => {
        name => WAN_IN,
      },
      out => {
        name => WAN_LOCAL,
      },
    },
    speed => 'auto',
  }
}

This type provides Puppet with the capabilities to manage interfaces on an edgeos router.
The configuration in params resembles the notation of the edgeos configuration language, with
the exception that we use arrays instead of repeating a parameter.
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    type: {
      type: 'String',
      desc: 'Interface type, i.e. "ethernet"',
    },
    params: {
      type: 'Array[String]',
      desc: 'List of type specific parameters',
    },
  },
)
