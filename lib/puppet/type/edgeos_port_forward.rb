# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'edgeos_port_forward',
  docs: <<-EOS,
@summary a edgeos_port_forward type
@example
edgeos_port_forward { 'auto-firewall':
  state => 'enable',
}
edgeos_port_forward { 'lan-interface':
  value => 'switch0',
}

This type provides Puppet with the capabilities to manage port forward configuration
EOS
  features: ['remote_resource'],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the port forwarding feature.',
      behaviour: :namevar,
    },
    state: {
      type:     'Enum[enable, disable]',
      desc:     'Wether the feature is enabled or disabled',
    },
    value: {
      type:     'String',
      desc:     'The value of the feature',
    },
  },
)
